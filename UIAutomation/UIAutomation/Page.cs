﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Web;

namespace UIAutomation
{
    public static class PageExt
    {
        public static TPage ToPage<TPage>(this ISiteUrl url, params OptionTypes[] options)
            where TPage : Page, new()
        {
            return Page.NavigateToPage<TPage>(url.ToUrl(), options);
        }

        public static TPage ToPage<TPage>(this ISiteUrl url, IWebDriver driver)
            where TPage : Page, new()
        {
            return Page.NavigateToPage<TPage>(url.ToUrl(), driver);
        }

        public static TPage ToPage<TPage>(string url)
           where TPage : Page, new()
        {
            return Page.NavigateToPage<TPage>(url);
        }
    }

    public class Page
    {
        public Page()
        { }

        public IWebDriver Driver { get; private set; }

        public SectionFactory Factory { get; protected set; }

        public Head Head => Factory.Load<Head>();

        public string Title => Driver.Title;

        public Uri Url => new Uri(HttpUtility.UrlDecode(Driver.Url));

        public static TPage NavigateToPage<TPage>(string url, params OptionTypes[] options)
                                                           where TPage : Page, new()
        {
            return NavigateToPage<TPage>(new Uri(url), options);
        }

        public static TPage NavigateToPage<TPage>(Uri url, params OptionTypes[] options)
            where TPage : Page, new()
        {
            return NavigateToPage<TPage>(url, DriverFactory.GetWebDriverWithOptions(options));
        }

        public static TPage NavigateToPage<TPage>(Uri url, IWebDriver driver)
            where TPage : Page, new()
        {
            var element = driver.TryFindElement(By.TagName("html"));

            driver.Navigate().GoToUrl(url);

            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5.00));

            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete")
                                    && (element == null || element.IsStale()));

            var page = new TPage();
            page.SetDriver(driver);

            if (!page.Verified())
                throw new Exception("Incorrect Page returned.");

            return page as TPage;
        }

        public void SetDriver(IWebDriver driver)
        {
            Driver = driver;
            Factory = new SectionFactory(this);

            if (!Verified())
                throw new Exception("Incorrect Page returned.");
        }

        public virtual bool Verified()
        {
            if (string.IsNullOrEmpty(Title) || Title.Contains("Error 500") || Title.Contains("Not Found"))
                return false;

            return true;
        }
    }
}