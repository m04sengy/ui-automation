﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Web;

namespace UIAutomation
{
    public class Anchor : Section
    {
        public enum OpensIn
        {
            NewWindowOrTab,
            SameWindowOrTab
        }

        public override By FindBy => Anchor.By();

        public string Href { get; private set; }

        public OpensIn Open { get; private set; }

        public string Rel { get; private set; }

        public string Title { get; private set; }

        public static By By()
        {
            return OpenQA.Selenium.By.TagName("a");
        }

        public bool Equals(Uri other)
        {
            var url = ToUrl();

            if (!url.Host.Equals(other.Host, StringComparison.InvariantCultureIgnoreCase))
                return false;

            if (!url.LocalPath.Equals(other.LocalPath, StringComparison.InvariantCultureIgnoreCase))
                return false;

            return QueryStringsAreEqual(url.Query, other.Query);
        }

        /// <summary>
        /// When a Anchor's Href query string is contextual, it means that the query string of the Page
        /// is the same as the query string of the Href. An example would be breadcrumbs and popular areas
        /// on Result Pages.
        /// </summary>
        /// <returns></returns>
        public bool QueryStringIsContextual(bool emptyQueryStringsAreContextual = false)
        {
            Uri url = ToUrl();

            if (string.IsNullOrEmpty(Page.Url.Query) && string.IsNullOrEmpty(url.Query))
                return emptyQueryStringsAreContextual;

            return QueryStringsAreEqual(Page.Url.Query, url.Query);
        }

        public Uri ToUrl()
        {
            return new Uri(Href);
        }

        protected override void OnLoad()
        {
            base.OnLoad();
            Href = WebElement.GetAttribute("href");
            Title = WebElement.GetAttribute("title");
            Rel = WebElement.GetAttribute("rel");
            Open = WebElement.GetAttribute("target") == "_blank" ? OpensIn.NewWindowOrTab : OpensIn.SameWindowOrTab;
        }

        private bool QueryStringsAreEqual(string query1, string query2)
        {
            var q1 = HttpUtility.ParseQueryString(query1);
            var q2 = HttpUtility.ParseQueryString(query2);

            if (q1.Count != q2.Count)
                return false;

            foreach (var key in q1.AllKeys)
            {
                if (q1[key].Contains(","))
                {
                    if (!q2[key].Contains(","))
                    {
                        return false;
                    }
                    var q1Array = q1[key].Split(new char[] { ',' });
                    var q2Array = q2[key].Split(new char[] { ',' });
                    if (q1Array.Length != q2Array.Length) return false;
                    var q1Lookup = new HashSet<string>(q1Array);
                    foreach (var q2Item in q2Array)
                    {
                        if (!q1Lookup.Contains(q2Item))
                        {
                            return false;
                        }
                    }
                }
                else if (!q1[key].Equals(q2[key], StringComparison.InvariantCultureIgnoreCase))
                    return false;
            }

            return true;
        }
    }
}