﻿using OpenQA.Selenium;

namespace UIAutomation
{
    public class Italic : Section
    {
        public string ClassString { get; private set; }

        public override By FindBy => Anchor.By();

        public static By By()
        {
            return OpenQA.Selenium.By.TagName("i");
        }

        protected override void OnLoad()
        {
            base.OnLoad();
            ClassString = WebElement.GetAttribute("class");
        }
    }
}