﻿using OpenQA.Selenium;
using System.Web;

namespace UIAutomation
{
    public class Meta : Section
    {
        public string Content { get; private set; }

        public override By FindBy => FindsBy();

        public string Name { get; private set; }

        public static By FindsBy()
        {
            return By.TagName("meta");
        }

        protected override void OnLoad()
        {
            Name = WebElement.GetAttribute("name");
            Content = HttpUtility.HtmlDecode(WebElement.GetAttribute("content"));
        }
    }
}