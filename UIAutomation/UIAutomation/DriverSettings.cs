﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace UIAutomation
{
    [Flags]
    public enum OptionTypes
    {
        None = 0,
        DontDownloadImages = 1,
        DontDownloadJavascript = 2,
        MobileEmulation = 4,
        //Remember to use powers of 2
        //NextOption = 4, etc..
        //NextOption = 8, etc..
    }

    public class DriverSettings
    {
        private OptionTypes _options;

        public DriverSettings(IEnumerable<OptionTypes> options) :
            this()
        {
            foreach (var o in options ?? new OptionTypes[0])
            {
                _options = _options | o;
            }
        }

        public DriverSettings()
        {
            _options = OptionTypes.None;
        }

        private DriverSettings(OptionTypes option)
        {
            _options = option;
        }

        public static DriverSettings Default()
        {
            return new DriverSettings();
        }

        public static DriverSettings LoadFromConfig()
        {
            DriverSettings options = new DriverSettings();

            if (bool.Parse(ConfigurationManager.AppSettings["DontDownloadImages"]))
                options.ApplyOptions(OptionTypes.DontDownloadImages);

            if (bool.Parse(ConfigurationManager.AppSettings["DontDownloadJavascript"]))
                options.ApplyOptions(OptionTypes.DontDownloadJavascript);

            if (bool.Parse(ConfigurationManager.AppSettings["MobileEmulation"]))
                options.ApplyOptions(OptionTypes.MobileEmulation);

            return options;
        }

        public void ApplyOptions(OptionTypes option, params OptionTypes[] options)
        {
            _options = _options | option;

            foreach (var o in options ?? new OptionTypes[0])
            {
                _options = _options | o;
            }
        }

        public DriverSettings Clone(DriverSettings settings)
        {
            return new DriverSettings(settings._options);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as DriverSettings);
        }

        public bool Equals(DriverSettings other)
        {
            if (other == null)
                return false;

            if (object.ReferenceEquals(this, other))
                return true;

            return this._options == other._options;
        }

        public override int GetHashCode()
        {
            return _options.GetHashCode();
        }

        public ChromeOptions ToOptions()
        {
            var chromeOptions = new ChromeOptions();

            if (HasOption(OptionTypes.DontDownloadImages))
                chromeOptions.AddUserProfilePreference("profile.managed_default_content_settings.images", 2);

            if (HasOption(OptionTypes.DontDownloadJavascript))
                chromeOptions.AddUserProfilePreference("profile.managed_default_content_settings.javascript", 2);

            if (HasOption(OptionTypes.MobileEmulation))
            {
                chromeOptions.EnableMobileEmulation("Google Nexus 5");
            }

            return chromeOptions;
        }

        private bool HasOption(OptionTypes option)
        {
            return (_options & option) == option;
        }
    }
}