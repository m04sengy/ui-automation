﻿using OpenQA.Selenium;
using System;

namespace UIAutomation
{
    public class Link : Section
    {
        public override By FindBy => By();

        public static By By()
        {
            return OpenQA.Selenium.By.TagName("link");
        }

        public string Href { get; private set; }

        public bool IsCanonical => Rel.Equals("canonical");

        public string Rel { get; private set; }

        public string Title { get; private set; }

        public Uri ToUrl()
        {
            return new Uri(Href);
        }

        protected override void OnLoad()
        {
            Href = WebElement.GetAttribute("href");
            Title = WebElement.GetAttribute("title");
            Rel = WebElement.GetAttribute("rel");
        }
    }
}