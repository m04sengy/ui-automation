﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UIAutomation.Blog_Insider.Urls;

namespace UIAutomation
{
    public class Site : ISiteUrl
    {
        public Site(Uri url, IPAddress ipAddress)
        {
            Url = url;
            IpAddress = ipAddress;
        }
        
        public Uri Url { get; private set; }

        public IPAddress IpAddress { get; private set; }

        public HomePageUrls HomePage()
        {
            return new HomePageUrls(Url);
        }

        ISiteUrl ISiteUrl.Clone()
        {
            throw new NotImplementedException();
        }

        string ISiteUrl.ToDecodedString()
        {
            throw new NotImplementedException();
        }

        Uri ISiteUrl.ToUrl()
        {
            throw new NotImplementedException();
        }
    }
}
