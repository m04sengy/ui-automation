﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Linq;
using System.Web;

namespace UIAutomation
{
    public static class WebElementExt
    {
        public static string TextContent(this IWebElement element)
        {
            return element.GetAttribute("textContent");
        }
    }

    public abstract class Section
    {
        protected SectionFactory Factory;
        private Page _page;
        private IWebElement _webElement;

        public virtual By FindBy
        {
            get
            {
                throw new Exception("Section does have a FindBy.");
            }
        }

        public Page Page
        {
            get
            {
                if (_page == null)
                    throw new Exception("Section has not been loaded.");

                return _page;
            }
        }

        public virtual string Text => HttpUtility.HtmlDecode(WebElement.Text);

        /// <summary>
        /// Retrieves the text of an element if it is hidden
        /// </summary>
        public virtual string TextContent => WebElement.TextContent();

        public bool Visible => _webElement.Displayed;

        public IWebElement WebElement
        {
            get
            {
                if (_webElement == null)
                    throw new Exception("Section has not been loaded.");

                return _webElement;
            }
        }

        public virtual void Click()
        {
            WebElement.Click();
        }

        public virtual TPage Click<TPage>()
         where TPage : Page, new()
        {
            Actions actions = new Actions(this.Page.Driver);

            actions.MoveToElement(WebElement).Click().Perform();

            TPage page = new TPage();
            page.SetDriver(this.Page.Driver);

            return page;
        }

        public void Load(Page page, IWebElement webElement)
        {
            if (_webElement != null)
                throw new Exception("Section has been loaded already. Please initialise a new Section.");

            _webElement = webElement;
            _page = page;

            Factory = new SectionFactory(_page, _webElement);

            OnLoad();
        }

        public virtual void MoveTo()
        {
            Actions action = new Actions(_page.Driver);
            action.MoveToElement(_webElement);
            action.Build().Perform();
            WaitFor(5, () => Visible);
        }

        public virtual TPage OpenInNewTab<TPage>(Uri url, out string currentTab) where TPage : Page, new()
        {
            currentTab = Page.Driver.CurrentWindowHandle;

            var currentTabs = Page.Driver.WindowHandles;

            var script = string.Format("window.open('{0}', '_blank');", url);
            var jsExecutor = (IJavaScriptExecutor)Page.Driver;
            jsExecutor.ExecuteScript(script);

            var newTab = Page.Driver.WindowHandles.Except(currentTabs).First();

            Page.Driver.SwitchTo().Window(newTab);

            TPage page = new TPage();
            page.SetDriver(this.Page.Driver);

            return page;
        }

        public virtual void OpenInNewTab<TPage>(Uri url, Action<TPage> perform) where TPage : Page, new()
        {
            string currentTab = null;

            try
            {
                TPage page = OpenInNewTab<TPage>(url, out currentTab);

                perform(page);

                page.Driver.Close();
                page.Driver.SwitchTo().Window(currentTab);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override string ToString()
        {
            return WebElement.GetAttribute("innerHTML").Trim();
        }

        public TSection TryFetchParent<TSection>(string expectedTag) where TSection : Section, new()
        {
            var element = Factory.TryLoad<TSection>(By.XPath(".."), this);
            if (element != null && !string.Equals(element.WebElement.TagName, expectedTag))
            {
                return null;
            }
            return element;
        }

        public virtual TPage UrlClick<TPage>() where TPage : Page, new()
        {
            Actions actions = new Actions(this.Page.Driver);

            actions.MoveToElement(WebElement).Click().Perform();

            WebDriverWait wait = new WebDriverWait(_page.Driver, TimeSpan.FromSeconds(3));
            wait.Until(driver1 => ((IJavaScriptExecutor)Page.Driver).ExecuteScript("return document.readyState").Equals("complete"));

            TPage page = new TPage();
            page.SetDriver(this.Page.Driver);

            return page;
        }

        public void WaitToBeVisible()
        {
            WaitForElementToBeVisible(FindBy);
        }

        protected virtual void OnLoad()
        {
        }

        protected T WaitFor<T>(Func<T> waitForCondition)
        {
            return WaitFor(3, waitForCondition);
        }

        protected T WaitFor<T>(double timeoutInSeconds, Func<T> waitForCondition)
        {
            WebDriverWait wait = new WebDriverWait(_page.Driver, TimeSpan.FromSeconds(timeoutInSeconds));

            return wait.Until((driver) => waitForCondition());
        }

        protected void WaitForElementToBeClickable(IWebElement element)
        {
            WaitFor(() => element.Displayed && element.Enabled);
        }

        protected void WaitForElementToBeVisible(By by)
        {
            WaitFor(() => WebElement.FindElement(by).Displayed);
        }
    }
}