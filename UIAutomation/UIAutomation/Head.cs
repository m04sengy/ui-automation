﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace UIAutomation
{
    public class Head : Section
    {
        public override By FindBy => By.TagName("head");

        public IEnumerable<Link> Links => Factory.LoadCollection<Link>(Link.By());

        public IEnumerable<Meta> MetaTag => Factory.LoadCollection<Meta>(Meta.FindsBy());
    }
}