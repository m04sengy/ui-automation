﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UIAutomation
{
    public class SectionFactory
    {
        private readonly ISearchContext _context;
        private readonly Page _page;
        private readonly Dictionary<string, IList> _sectionCollections;
        private readonly Dictionary<string, Section> _sections;

        public SectionFactory(Page page) :
            this(page, page.Driver)
        { }

        public SectionFactory(Page page, ISearchContext context)
        {
            _page = page;
            _context = context;

            _sections = new Dictionary<string, Section>();
            _sectionCollections = new Dictionary<string, IList>();
        }

        public TSection Load<TSection>()
            where TSection : Section, new()
        {
            return Load<TSection>(typeof(TSection).Name);
        }

        public TSection Load<TSection>(string name)
            where TSection : Section, new()
        {
            if (_sections.ContainsKey(name))
                return _sections[name] as TSection;

            return Load(new TSection(), name);
        }

        private TSection Load<TSection>(TSection section, string name)
            where TSection : Section
        {
            IWebElement fElement = null;
            int tries = 0;
            do
            {
                try
                {
                    fElement = _context.FindElement(section.FindBy);
                }
                catch (NoSuchElementException) { tries++; }
            }
            while (tries < 3 && fElement == null);
            if (fElement == null) throw new NoSuchElementException($"Cannot find element in {(_context as IWebDriver).PageSource}");
            return Load(section, fElement, name);
        }

        private TSection Load<TSection>(TSection section, IWebElement element, string name)
            where TSection : Section
        {
            section.Load(_page, element);

            _sections.Add(name, section);

            return section;
        }

        public List<TSection> LoadCollection<TSection>(By by)
            where TSection : Section, new()
        {
            return LoadCollection<TSection>(typeof(TSection).Name, by);
        }

        public List<TSection> LoadCollection<TSection>(string name, By by)
            where TSection : Section, new()
        {
            if (_sectionCollections.ContainsKey(name))
                return _sectionCollections[name] as List<TSection>;

            List<TSection> items = LoadCollection<TSection>(name, _context.FindElements(by));

            _sectionCollections.Add(name, items);

            return items;
        }

        private List<TSection> LoadCollection<TSection>(string name, ReadOnlyCollection<IWebElement> elements)
            where TSection : Section, new()
        {
            List<TSection> items = new List<TSection>();

            int element = 0;
            foreach (var webElement in elements)
            {
                items.Add(Load<TSection>(new TSection(), webElement, name + "[" + element.ToString() + "]"));
                element++;
            }

            return items;
        }

        public TSection TryLoad<TSection>(By by, Section section = null)
                                             where TSection : Section, new()
        {
            return TryLoad<TSection>(by, typeof(TSection).Name, section);
        }

        public TSection TryLoad<TSection>(By by, string name, Section section = null)
            where TSection : Section, new()
        {
            ISearchContext context = _context;

            if (section != null)
                context = section.WebElement;

            return TryLoad<TSection>(by, name, context);
        }

        private TSection TryLoad<TSection>(By by, string name, ISearchContext context)
                                    where TSection : Section, new()
        {
            try
            {
                if (_sections.ContainsKey(name))
                    return _sections[name] as TSection;

                return Load<TSection>(new TSection(), context.FindElement(by), name);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}