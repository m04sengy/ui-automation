﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UIAutomation
{
    public static class DriverFactory
    {
        public static readonly DriverSettings _default = DriverSettings.LoadFromConfig();
        public static readonly Dictionary<DriverSettings, IWebDriver> _drivers = new Dictionary<DriverSettings, IWebDriver>();

        public static void Close()
        {
            lock (_drivers)
            {
                IWebDriver driver;
                foreach (var setting in new List<DriverSettings>(_drivers.Keys))
                {
                    driver = _drivers[setting];

                    driver.Close();
                    driver.Dispose();

                    _drivers.Remove(setting);
                }
            }
        }

        public static IWebDriver GetWebDriver(DriverSettings settings = null)
        {
            settings = settings == null ? _default : settings;

            if (!_drivers.ContainsKey(settings))
            {
                lock (_drivers)
                {
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    string path = System.IO.Path.GetDirectoryName(assembly.Location);
                    if (!_drivers.ContainsKey(settings))
                    {
                        var driver = new ChromeDriver(path, settings.ToOptions());
                        driver.Manage().Window.Maximize();
                        _drivers.Add(settings, driver);
                    }
                }
            }

            return _drivers[settings];
        }

        public static IWebDriver GetWebDriverWithOptions(params OptionTypes[] options)
        {
            return GetWebDriver(options != null && options.Any() ? new DriverSettings(options) : null);
        }

        public static void Initialise()
        {
            GetWebDriver(_default);
        }
    }
}