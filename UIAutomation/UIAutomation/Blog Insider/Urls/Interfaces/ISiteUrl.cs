﻿using System;

namespace UIAutomation
{
    public interface ISiteUrl
    {
        ISiteUrl Clone();

        string ToDecodedString();

        Uri ToUrl();
    }

    public interface ISiteUrlClone<TInterface> :
        ISiteUrl
    {
        TInterface CloneAsSameType();
    }
}