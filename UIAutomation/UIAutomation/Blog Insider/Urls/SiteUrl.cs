﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace UIAutomation
{
    public abstract class SiteUrl : ISiteUrl
    {
        protected NameValueCollection _queryStringValues;
        protected UriBuilder _url;

        protected SiteUrl(Uri url)
        {
            _queryStringValues = new NameValueCollection();
            _url = new UriBuilder(url);
        }

        public virtual ISiteUrl Clone()
        {
            return this.MemberwiseClone() as SiteUrl;
        }

        public string ToDecodedString()
        {
            return HttpUtility.UrlDecode(ToString());
        }

        public override string ToString()
        {
            return ToUrl().ToString();
        }

        public virtual Uri ToUrl()
        {
            return _url.Uri;
        }

        protected void AddQueryStringValue(string name, string value)
        {
            _queryStringValues.Add(name, value);
        }

        protected string GenerateQueryString()
        {
            if (_queryStringValues == null || _queryStringValues.Count == 0)
            {
                return null;
            }

            return ToQueryString(_queryStringValues);
        }

        protected void SetUrl(string path, string queryString = null)
        {
            _url.Path = path;
            _url.Query = queryString;
        }

        protected string ToQueryString(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), Uri.EscapeDataString(value))) // HttpUtility.UrlEncode(value)))
                .ToArray();
            return string.Join("&", array);
        }
    }
}