﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIAutomation.Blog_Insider.Urls
{
    public class HomePageUrls : SiteUrl
    {
        public HomePageUrls(Uri url) : base(url)
        {
        }

        public override Uri ToUrl()
        {
            SetUrl("/");
            return base.ToUrl();
        }
    }
}
