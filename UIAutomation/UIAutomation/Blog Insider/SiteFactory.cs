﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UIAutomation.Blog_Insider
{
    public class SiteFactory
    {
        private static readonly SiteFactory _site = new SiteFactory();
        private readonly string _uriScheme;
        private readonly string _siteFeatureID;
        private readonly string _siteIpAddress;
        private readonly string _siteDomain;
        private readonly bool _isFeatureEnv;
        private readonly Dictionary<string, Site> _sites;

        public SiteFactory()
        {
            _sites = new Dictionary<string, Site>();

            _uriScheme = ConfigurationManager.AppSettings["UriScheme"];
            _siteFeatureID = ConfigurationManager.AppSettings["FeatureID"];
            _siteDomain = ConfigurationManager.AppSettings["BlogDomain"];
            _isFeatureEnv = Boolean.Parse(ConfigurationManager.AppSettings["IsFeatureEnv"]);
            _siteIpAddress = ConfigurationManager.AppSettings["BlogIpAddress"];
        }

        public static Site Blog => _site.GetSite(LocalSite.Blog);

        public Site GetSite(LocalSite site)
        {
            lock (_sites)
            {
                if (!_sites.ContainsKey(site.Name))
                {
                    _sites.Add(site.Name, new Site(GetUrl(), GetIPAddress()));
                }
                return _sites[site.Name];
            }
        }

        private Uri GetUrl()
        {
            if (_isFeatureEnv)
                return new Uri($"{_uriScheme}://{_siteFeatureID}-{_siteDomain}");
            return new Uri($"{_uriScheme}://{_siteDomain}");
        }

        private IPAddress GetIPAddress()
        {
            return IsValidIpAddress(_siteIpAddress);
        }

        private IPAddress IsValidIpAddress(string address)
        {
            IPAddress ipAddress;
            var isValidIpAddress = IPAddress.TryParse(address, out ipAddress);
            if (!isValidIpAddress)
                throw new Exception($"{address} is not a valid IP Address");
            return ipAddress;
        }

        public class LocalSite
        {
            public static readonly LocalSite Blog = new LocalSite("Blog");

            public LocalSite(string name)
            {
                Name = name;
            }
            public string Name { get; private set; }
        }

        public enum EnvironmentType
        {
            Feature,
            Test,
            Live
        }
    }
}
