﻿using OpenQA.Selenium;

namespace UIAutomation.Blog_Insider.Templates.Default
{
    public class Header : Section
    {
        public override By FindBy => By.CssSelector("head");

        protected override void OnLoad()
        {
            base.OnLoad();
        }
    }
}