﻿using UIAutomation;

namespace UIAutomation.Blog_Insider.Templates.Default
{
    public class WebPage : Page
    {
        public Body Body => Factory.Load<Body>();

        public Footer Footer => Factory.Load<Footer>();
        
        public Header Header => Factory.Load<Header>();
    }
}