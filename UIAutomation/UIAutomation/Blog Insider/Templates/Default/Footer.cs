﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace UIAutomation.Blog_Insider.Templates.Default
{
    public class Footer : Section
    {
        public override By FindBy => By.CssSelector(".footer.clearfix");

        protected override void OnLoad()
        {
            base.OnLoad();
        }
    }
}