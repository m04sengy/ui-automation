﻿using OpenQA.Selenium;

namespace UIAutomation.Blog_Insider.Templates.Default
{
    public class Body : Section
    {
        public override By FindBy => By.CssSelector("body");

        protected override void OnLoad()
        {
            base.OnLoad();
        }
    }
}