﻿using OpenQA.Selenium;

namespace UIAutomation
{
    public static class DriverExtensions
    {
        public static bool IsStale(this IWebElement driver)
        {
            try
            {
                driver.FindElement(By.TagName("html"));
            }
            catch (StaleElementReferenceException)
            {
                return true;
            }
            return false;
        }

        public static IWebElement TryFindElement(this IWebDriver driver, By by)
        {
            IWebElement element;
            try
            {
                element = driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                element = null;
            }
            return element;
        }
    }
}