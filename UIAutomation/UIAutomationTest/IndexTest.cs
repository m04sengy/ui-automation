﻿using NUnit.Framework;
using System.Configuration;
using UIAutomation;
using UIAutomation.Blog_Insider;
using UIAutomation.Blog_Insider.Templates.Default;

namespace UIAutomationTest
{
    [TestFixture]
    public class TestClass
    {
        [Test]
        public void NagivateToBlogHomePage()
        {
            //Arrange
            var pageUrl = SiteFactory.Blog.HomePage();

            //Act
            var homePage = pageUrl.ToPage<HomePage>();
            var body = homePage.Body;

            //Assert
            Assert.IsTrue(homePage.Url.Equals("https://blog.betway.com/"));
        }

        [TearDown]
        public void TearDown()
        {
            DriverFactory.Close();
        }
    }
}